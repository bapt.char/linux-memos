# Debian 10 Memos

### Enable SSH root login
It seems like Debian 10 installed with OVH has by default the SSH root login disabled. Let's edit `/etc/ssh/sshd_config`:
```sh
$ sudo nano /etc/ssh/sshd_config
````
Replace `PermitRootLogin prohibit-password` with `PermitRootLogin yes`. Save with CTRL+X then Y. Then we restart SSH service:
```sh
$ sudo /etc/init.d/ssh restart
````

### Change user password
```sh
$ sudo passwd <user>
````

### Speedtest CLI
```sh
$ apt install speedtest-cli
$ speedtest-cli --share
````

### Apache 2
Update dependencies and install apache2:
```sh
$ apt update && apt upgrade
$ apt install apache2
````
If we need to restart apache service:
```sh
$ /etc/init.d/apache2 restart
```

### PHP 7.4 (Using Sury)
```sh
$ sudo apt -y install lsb-release apt-transport-https ca-certificates 
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo apt update
$ sudo apt install php7.4 php7.4-{bcmath,bz2,intl,gd,mbstring,mysql,zip}
````

### Certbot (for Apache 2)
We start by installing certbot and a python dependency:
```sh
$ sudo apt install certbot python-certbot-apache
```
Now let's get and install certificates:
```sh
$ sudo certbot --apache -d domain -d www.domain
```
We will need to provide an email address and accept transport
Then we must select the HTTPS (SSL) conf (usually 2)
If we are using cloudflare, we should NOT redirect HTTP to HTTPS automatically, as CF will do that for us.
```sh
Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
...
Congratulations!
```
Now we can test automatic renewal:
```sh
$ sudo certbot renew --dry-run
...
Congratulations, all renewals succeeded.
```

### Maria-DB
#### Install mariadb-server
```sh
$ sudo apt install mariadb-server
```
Secure installation:
```sh
$ sudo mysql_secure_installation
...
Thanks for using MariaDB!
```
#### Create user
```sh
$ sudo mariadb
> GRANT ALL ON *.* TO 'user'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
> exit
```

### Installing latest phpMyAdmin from source
#### Download
Let's start by downloading latest phpMyAdmin:
```sh
$ sudo wget https://files.phpmyadmin.net/phpMyAdmin/5.0.2/phpMyAdmin-5.0.2-all-languages.tar.gz
```
Now untar the archive:
```sh
$ sudo tar xvf phpMyAdmin-5.0.2-all-languages.tar.gz
```
Delete the archive:
```sh
$ sudo rm phpMyAdmin-5.0.2-all-languages.tar.gz
```
Move the directory to /usr/share/phpmyadmin:
```sh
$ sudo mv phpMyAdmin-5.0.2-all-languages /usr/share/phpmyadmin
```
#### Configuring phpMyAdmin
Create phpmyadmin temp directory:
```sh
$ sudo mkdir -p /var/lib/phpmyadmin/tmp
```
Give permissions to that directory:
```sh
$ sudo chown -R www-data:www-data /var/lib/phpmyadmin
```
Copy default config file:
```sh
$ sudo cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php
```
Edit config file:
```sh
$ sudo nano /usr/share/phpmyadmin/config.inc.php
```
We need to set a blowfish secret key (Generator [here](https://phpsolved.com/phpmyadmin-blowfish-secret-generator/))
```
$cfg['blowfish_secret'] = '/W.=joP/i,Qf2{Ck19DUQ20nO04,JNya';
```
Uncomment theses two line and set a strong password:
```
$cfg['Servers'][$i]['controluser'] = 'pma';
$cfg['Servers'][$i]['controlpass'] = 'strong-password';
```
Uncomment tables names:
```
/* Storage database and tables */
$cfg['Servers'][$i]['pmadb'] = 'phpmyadmin';
$cfg['Servers'][$i]['bookmarktable'] = 'pma__bookmark';
$cfg['Servers'][$i]['relation'] = 'pma__relation';
$cfg['Servers'][$i]['table_info'] = 'pma__table_info';
$cfg['Servers'][$i]['table_coords'] = 'pma__table_coords';
$cfg['Servers'][$i]['pdf_pages'] = 'pma__pdf_pages';
$cfg['Servers'][$i]['column_info'] = 'pma__column_info';
$cfg['Servers'][$i]['history'] = 'pma__history';
$cfg['Servers'][$i]['table_uiprefs'] = 'pma__table_uiprefs';
$cfg['Servers'][$i]['tracking'] = 'pma__tracking';
$cfg['Servers'][$i]['userconfig'] = 'pma__userconfig';
$cfg['Servers'][$i]['recent'] = 'pma__recent';
$cfg['Servers'][$i]['favorite'] = 'pma__favorite';
$cfg['Servers'][$i]['users'] = 'pma__users';
$cfg['Servers'][$i]['usergroups'] = 'pma__usergroups';
$cfg['Servers'][$i]['navigationhiding'] = 'pma__navigationhiding';
$cfg['Servers'][$i]['savedsearches'] = 'pma__savedsearches';
$cfg['Servers'][$i]['central_columns'] = 'pma__central_columns';
$cfg['Servers'][$i]['designer_settings'] = 'pma__designer_settings';
$cfg['Servers'][$i]['export_templates'] = 'pma__export_templates';
```
Set temp directory path:
```
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';
$cfg['TempDir'] = '/var/lib/phpmyadmin/tmp';
```
Save with CTRL+X then Y. Then we need to import phpmyadmin database's tables:
```sh
$ sudo mariadb < /usr/share/phpmyadmin/sql/create_tables.sql
```
Create pma user (defined earlier)
```sh
$ sudo mariadb
$ GRANT SELECT, INSERT, UPDATE, DELETE ON phpmyadmin.* TO 'pma'@'localhost' IDENTIFIED BY 'strong-password';
```
#### Configuring Apache 2 to work with phpMyAdmin
Create phpMyAdmin config file for apache
```sh
$ sudo nano /etc/apache2/conf-available/phpmyadmin.conf
```
Here is the default config:
```
# phpMyAdmin default Apache configuration

Alias /phpmyadmin /usr/share/phpmyadmin

<Directory /usr/share/phpmyadmin>
    Options SymLinksIfOwnerMatch
    DirectoryIndex index.php

    <IfModule mod_php5.c>
        <IfModule mod_mime.c>
            AddType application/x-httpd-php .php
        </IfModule>
        <FilesMatch ".+\.php$">
            SetHandler application/x-httpd-php
        </FilesMatch>

        php_value include_path .
        php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
        php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/
        php_admin_value mbstring.func_overload 0
    </IfModule>
    <IfModule mod_php.c>
        <IfModule mod_mime.c>
            AddType application/x-httpd-php .php
        </IfModule>
        <FilesMatch ".+\.php$">
            SetHandler application/x-httpd-php
        </FilesMatch>

        php_value include_path .
        php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
        php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/
        php_admin_value mbstring.func_overload 0
    </IfModule>

</Directory>

# Authorize for setup
<Directory /usr/share/phpmyadmin/setup>
    <IfModule mod_authz_core.c>
        <IfModule mod_authn_file.c>
            AuthType Basic
            AuthName "phpMyAdmin Setup"
            AuthUserFile /etc/phpmyadmin/htpasswd.setup
        </IfModule>
        Require valid-user
    </IfModule>
</Directory>

# Disallow web access to directories that don't need it
<Directory /usr/share/phpmyadmin/templates>
    Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/libraries>
    Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/setup/lib>
    Require all denied
</Directory>
```
Save with CTRL+X and Y. Now we need to enable this new config:
```sh
$ sudo a2enconf phpmyadmin.conf
```
Reload apache:
```sh
$ sudo systemctl reload apache2
```
We can now access http://domain.com/phpmyadmin
#### Securing phpMyAdmin
Edit our phpmyadmin apache config file:
```sh
$ sudo nano /etc/apache2/conf-available/phpmyadmin.conf
```
Add an `AllowOverride All` directive within the `<Directory /usr/share/phpmyadmin>`
```
<Directory /usr/share/phpmyadmin>
    Options SymLinksIfOwnerMatch
    DirectoryIndex index.php
    AllowOverride All

    <IfModule mod_php5.c>
        ...
```
Save with CTRL+X then Y. Restart apache to apply changes:
```sh
$ sudo systemctl restart apache2
```
Create htaccess file:
```sh
$ sudo nano /usr/share/phpmyadmin/.htaccess
```
Enter this content:
```
AuthType Basic
AuthName "Restricted Files"
AuthUserFile /usr/share/phpmyadmin/.htpasswd
Require valid-user
```
Save with CTRL+X then Y. Now we want an user (you will be asked to provide a password):
```sh
$ sudo htpasswd -c /usr/share/phpmyadmin/.htpasswd <username>
```
Optionnal: if we want to add an user (ommit -c flag):
```sh
$ sudo htpasswd /etc/phpmyadmin/.htpasswd additionaluser
```
##### Our phpMyAdmin panel is now secured!